import { Component, OnInit } from '@angular/core';
import { NavController } from  '@ionic/angular';
import { LogicalService } from './../../services/logical/logical.service'; //import logical service

@Component({
  selector: 'app-youtube-feed',
  templateUrl: './youtube-feed.page.html',
  styleUrls: ['./youtube-feed.page.scss'],
})

//MAI BRANCH ORIGIN
export class YoutubeFeedPage implements OnInit {
    //list of videos to use on view
    lstVideos=[];

    constructor(private logicalSrv: LogicalService, public navCtrl: NavController) { }

    ngOnInit() {
        //on initialize load the YT videos
        this.loadYT();
    }

    //Method to Load Videos from YT by calling logical service to retrieve the videos
    loadYT() {
      //getVideos method will return Observable so we subscribe to get response   
        this.logicalSrv.getVideos().subscribe((response) => {
         //we receive the videos on items property and we assign to our local list to show on view
          this.lstVideos = response["items"]; 

        }, (error) => {
          //error , we show a message 
          this.logicalSrv.showToast("Ups! Not able to get videos from YT", 1500);
      });
    }


    //Method to Like video, this event is fired by thumbs down
    likeVideo(video: any) {
      //show success message
      this.logicalSrv.showToast("You Liked a Video!", 1500);
      //add video to our liked video list by calling logical service 
      this.logicalSrv.addVideoLikeList(video);
      //update video model property to show on view thumbs-up or thmbs-down
      video.Like = true;
    }

    //Method to Dislike a video, this event is fired by thumbs up
    dislikeVideo(video: any) {
        //show success message
        this.logicalSrv.showToast("You Dislike a Video", 1500);
        //add video to our liked video list by calling logical service 
        this.logicalSrv.removeVideoLikeList(video);
        //update video model property to show on view thumbs-up or thmbs-down
        video.Like = false;
    } 
    //Method to navigate to the Liked Videos Page, this event is fired by button on right side of navbar
    navigateLikedVideos() {
        this.navCtrl.navigateForward("/liked-videos" );
    }
}
