import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; //import to make http request
import { environment } from '../../../environments/environment';  //import to access env constants
import { AngularFireAuth } from 'angularfire2/auth'; //import to access Angular Firebase Auth
import { auth } from 'firebase/app'; //import to access Auth firebase SKD using Angular Firebase Auth wrapper and Configuration

@Injectable({
  providedIn: 'root'
})
export class CommunicationsService {
  
    constructor(private http: HttpClient, public firebaseAuth: AngularFireAuth) { }
     
    // Method for login by calling firebase api using AngularFireAuth and sending email and password as parameters
    // @param email string
    // @param password string
    Login(email: string, password: string) : any{
       return this.firebaseAuth.auth.signInWithEmailAndPassword(email, password);
    };


    // Method to get videos for YT api url with YT api key (created in google developers)
    // We are using enviroment constants for YT api url and YT api key
    getVideos()   {
        return this.http.get(environment.YT_API_URL + environment.YT_API_KEY);
    };


}
