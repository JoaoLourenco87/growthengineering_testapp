**Project Information **

Create App with the following features:
	
- Login
	- Show YT Videos
		- Like Videos
	- Show Liked Videos
	
For Login we used Firebase Authentication:

Credentials:

		admin@admin.com
		123456

For YT Videos, we used YT API url provided.
	URL: 

https://www.googleapis.com/youtube/v3/playlistItems?part=snippet%2CcontentDetails%2Cstatus&maxResults=5&playlistId=UUTI5S0PqpgB0DbYgcgRU6QQ&key={YOUR_API_KEY} 
	

---

## Project Details

- Angular v7
- Ionic v4
- Cordova v8

---


## Main Development Steps

 

1. Create new ionic app
   
     1.1 #command_line ionic start growthengineering blank 
2. Create pages
   
     2.1 #command_line ionic generate page "pages/Login"
   
    2.2 #command_line ionic generate page "pages/Youtube Feed"
   
    2.3 #command_line ionic generate page "pages/Liked Videos"

3. Create modules
   
    3.1 #command_line ionic generate module "services/Logical"
   
    3.2 #command_line ionic generate module "services/Communications"
	
4. Create services
   
    4.1 #command_line ionic generate service "services/Logical/Logical" 
   
    4.2 #command_line ionic generate service "services/Communications/Communications"
	
5. Add dependecies
   
    5.1 #command_line npm install angularfire2
   
    5.2 #command_line npm install firebase 
   
    5.3 #command_line ionic cordova plugin add cordova-plugin-firebase-authentication (we really dont need, unless we going to try in device)

6. Add the required import's across the modules
7. Development Communications Service
8. Development Logical Service
9. Development Login Page
9. Development Youtube Videos Page
10. Development Youtube Liked Videos Page
11. Review & Add comments

---

## Run

From command line:
- npm install
- ionic serve

---

## Joao Lourenço to growthengineering
