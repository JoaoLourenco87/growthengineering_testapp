import { Injectable } from '@angular/core';
import { ToastController, LoadingController  } from '@ionic/angular';
import { CommunicationsService } from '../communications/communications.service';

@Injectable({
  providedIn: 'root'
})
export class LogicalService {

 
    private lstLikedVideos = [];
    private loading: any;

    constructor(private commSrv: CommunicationsService, private toastCtrl: ToastController, private loadingCtrl : LoadingController ) { }


     // Method to show toast message
    // @param message string
    // @param msduration number - in miliseconds ex: 2000 = 2sec
    async showToast(message: string, msduration : number) {

        var tmpToast = await this.toastCtrl.create({
            message: message,
            duration: msduration,
            position: 'top'
        });
        tmpToast.present();

    }
    // Method to present loading on the view
    async showLoading() {
        this.loading = await this.loadingCtrl.create({
            message: 'Please Wait..'
        });
        await this.loading.present(); 
    }
    // Method to dismiss loading from the view
    hideLoading() {
         this.loading.dismiss();
    }

    // Method for login by calling communications service
    // @param objLogin any
    //      @prop email string
    //      @prop password string - min 6 char's
    Login(objLogin: any): any {
        return this.commSrv.Login(objLogin.email,objLogin.password);
    };

    // Method to get YT Videos by calling communications service 
    // @returns list of videos 
    getVideos()  {
        return this.commSrv.getVideos();
    };

   // Method to get YT LIKED Videos
   // @returns list of videos
    getLikedVideos() {
        return this.lstLikedVideos;
    };

    // Method to add Video to list - in this case to add the video to Liked Videos list (lstLikedVideos)
    // @param video any - Video from list return in getVideos
    addVideoLikeList(video: any) {
        this.lstLikedVideos.push(video);
    };

    // Method to remove Video from list - in this case to remove from Liked Videos list (lstLikedVideos)
    // @param video any - Video from list return in getVideos
    removeVideoLikeList(video: any) {
        this.lstLikedVideos.splice(this.lstLikedVideos.indexOf(video),1);
    };

    

}
