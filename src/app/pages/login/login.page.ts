import { Component, OnInit } from '@angular/core';
import { NavController } from  '@ionic/angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms'; //import form components 
import { LogicalService } from './../../services/logical/logical.service'; //import Logical service 

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'] 
})

//MAI BRANCH ORIGIN
export class LoginPage implements OnInit {
    
    frmGroup: FormGroup; 
    //model for login to be used on view
    loginModel = {
        email: "",
        password : ""
    };

    constructor(private logicalService: LogicalService, public navCtrl: NavController) { }

    ngOnInit() {
        
         //on initialize create form group for validation purposes
        this.frmGroup = new FormBuilder().group({
            email: new FormControl('', Validators.compose([
                Validators.required,
                Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
            ])),
            password: new FormControl('', Validators.compose([
                Validators.required,
                Validators.minLength(6)])),
        });
    }

    // Make the login by calling Logical Service doLogin method
    // This event is fired by Login button, when form is valid based on formgroup validators
    doLogin() {
        this.logicalService.showLoading(); //show loading

        this.logicalService.Login(this.loginModel).then((response) => {

            // Login successful
            this.logicalService.hideLoading(); //hide loading
            this.logicalService.showToast("Login OK!", 3000); //show toast 
            this.navCtrl.navigateRoot("/youtube-feed"); //navigate and set new page as root 


        }, (error) => { 
            // Login error
            this.logicalService.hideLoading(); //hide loading
            this.logicalService.showToast("Login ERROR!",3000); //show toast 
        });

       
    }

}
