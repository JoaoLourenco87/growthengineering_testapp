import { Component, OnInit } from '@angular/core';
import { LogicalService } from './../../services/logical/logical.service'; //inject Logical service

@Component({
  selector: 'app-liked-videos',
  templateUrl: './liked-videos.page.html',
  styleUrls: ['./liked-videos.page.scss'],
})
export class LikedVideosPage implements OnInit {
    lstVideos = [];
    constructor(private srvLogical: LogicalService) { }

    ngOnInit() {
        //on initialize we will load liked videos
        this.loadYTLikedVideos();
    }

    //Method to Load Liked Videos calling logical service to retrieve the list of liked videos
    loadYTLikedVideos() {
        this.lstVideos = this.srvLogical.getLikedVideos();
    }
}
