import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommunicationsModule } from '../communications/communications.module';

@NgModule({
  declarations: [],
  imports: [
      CommonModule,
      CommunicationsModule
  ]
})
export class LogicalModule { }
