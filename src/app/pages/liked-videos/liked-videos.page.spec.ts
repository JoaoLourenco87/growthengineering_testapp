import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LikedVideosPage } from './liked-videos.page';

describe('LikedVideosPage', () => {
  let component: LikedVideosPage;
  let fixture: ComponentFixture<LikedVideosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LikedVideosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LikedVideosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
