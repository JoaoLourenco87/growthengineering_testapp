import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { LogicalModule } from './../../services/logical/logical.module'; 
import { LikedVideosPage } from './liked-videos.page';


const routes: Routes = [
  {
    path: '',
    component: LikedVideosPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LogicalModule, 
    RouterModule.forChild(routes)
  ],
  declarations: [LikedVideosPage]
})
export class LikedVideosPageModule {}
