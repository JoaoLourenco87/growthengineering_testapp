import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';  
import { AngularFireAuthModule } from 'angularfire2/auth';

@NgModule({
  declarations: [],
  imports: [
      CommonModule,
      AngularFireAuthModule
  ]
})
export class CommunicationsModule { }
