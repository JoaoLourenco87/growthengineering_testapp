import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { LogicalModule } from './../../services/logical/logical.module'; 
import { YoutubeFeedPage } from './youtube-feed.page';


const routes: Routes = [
  {
    path: '',
    component: YoutubeFeedPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LogicalModule,
    RouterModule.forChild(routes)
  ],
  declarations: [YoutubeFeedPage]
})
export class YoutubeFeedPageModule {}
