import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'youtube-feed', loadChildren: './pages/youtube-feed/youtube-feed.module#YoutubeFeedPageModule' },
  { path: 'liked-videos', loadChildren: './pages/liked-videos/liked-videos.module#LikedVideosPageModule' },
];
//MAI BRANCH ORIGIN
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
